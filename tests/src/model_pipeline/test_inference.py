import numpy as np

from src.model_pipeline import inference


def test_PredictionPipeline_get_predictions(monkeypatch):
    target_words = ["a", "b", "c", "d"]

    def mock_get_single_prediction(img, **kwargs):
        return target_words, np.ones((1, 20, 512)), np.ones((512, 49))

    transforms = inference.PredictionPipeline.get_default_transformations(
        image_resize=256, image_crop=224, image_normalization=[0.5, 0.5, 0.5], image_std=[0.5, 0.5, 0.5]
    )

    pred_pipeline = inference.PredictionPipeline(model=None, vocab=None, image_transform=transforms)
    pred_pipeline.get_single_prediction = mock_get_single_prediction

    img = np.ones((260, 260, 3), dtype=np.uint8)
    test_output = pred_pipeline.get_predictions(img=img)

    for target_word, (word, test_array) in zip(target_words, test_output):
        assert word == target_word
        assert isinstance(test_array, np.ndarray)
