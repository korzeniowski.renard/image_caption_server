import torch
from torch import nn
import torchvision.models as models

import config


class VggEncoder(nn.Module):
    def __init__(self, encoded_image_size):
        super().__init__()
        self.model = self.get_pretrained_model()
        self.adaptive_pool = nn.AdaptiveAvgPool2d((encoded_image_size, encoded_image_size))

    def get_torch_pretrained_model(self):
        model = models.vgg16(pretrained=True, progress=True)
        return model

    def get_pretrained_model(self):
        super().__init__()
        model = self.get_torch_pretrained_model()
        self.freeze_pretrained_layers(model)
        modules = (list(model.children())[:-1])
        model = nn.Sequential(*modules)
        return model

    def freeze_pretrained_layers(self, model):
        for param in model.parameters():
            param.requires_grad = False

    def forward(self, images):
        out = self.model(images)
        out = out.permute(0, 2, 3, 1)
        out = torch.flatten(out, start_dim=1, end_dim=2)
        return out


class AttentionLayer(nn.Module):
    def __init__(self, attention_dim, hidden_dim, feature_maps_nb):
        super().__init__()
        self.fm_fc = nn.Linear(feature_maps_nb, attention_dim)
        self.h_fc = nn.Linear(hidden_dim, attention_dim)
        self.att_fc = nn.Linear(attention_dim, 1)
        self.softmax = nn.Softmax(dim=1)
        self.relu = nn.ReLU()

    def forward(self, feature_maps, hidden_lstm_state):
        att_fm = self.fm_fc(feature_maps)
        att_h = self.h_fc(hidden_lstm_state)
        total_att = self.relu(att_h.unsqueeze(1) + att_fm)
        weight_map = self.att_fc(total_att).squeeze(-1)
        weight_map = self.softmax(weight_map)
        weighted_feature_maps = (feature_maps * weight_map.unsqueeze(2)).sum(1)
        return weighted_feature_maps


class AttentionDecoder(nn.Module):
    def __init__(self, vocab_size, feature_maps_nb, word_emb_dim, hidden_lstm_dim, attention_dim, voc_fc_dropout):
        super().__init__()
        self.word_embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=word_emb_dim)
        self.init_fc_h = nn.Linear(in_features=feature_maps_nb, out_features=hidden_lstm_dim)
        self.init_fc_c = nn.Linear(in_features=feature_maps_nb, out_features=hidden_lstm_dim)
        self.vocab_fc = nn.Linear(in_features=hidden_lstm_dim, out_features=vocab_size)
        self.lstm = nn.LSTM(input_size=feature_maps_nb+word_emb_dim, hidden_size=hidden_lstm_dim)
        self.attention_layer = AttentionLayer(
            attention_dim=attention_dim,
            hidden_dim=hidden_lstm_dim,
            feature_maps_nb=feature_maps_nb,
        )
        self.softmax = nn.Softmax(dim=1)
        self.dropout = nn.Dropout(voc_fc_dropout)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    def forward(self, feature_maps, text):
        pred_words_dists, hist_att_weights = [], []

        h, c = self.get_lstm_initial_state(feature_maps)
        text = self.add_bos_token(text)
        txt_embed = self.word_embeddings(text)

        for w_emb in txt_embed.permute(1, 0, 2):
            word_distribution, h, c, attn_weights = self.predict_single_word(w_emb, feature_maps, h, c)
            pred_words_dists.append(word_distribution)

        return torch.stack(pred_words_dists).permute(1, 0, 2)

    def sample(self, feature_maps):
        pred_words, hist_att_weights = [], []

        h, c = self.get_lstm_initial_state(feature_maps)
        bos = self.get_bos_idx_sample(bs=feature_maps.shape[0]).squeeze(1)
        w_emb = self.word_embeddings(bos)

        seq_len = 0
        while seq_len < config.max_pred_seq_len:
            word_distribution, h, c, attn_weights = self.predict_single_word(w_emb, feature_maps, h, c)
            single_pred_word = self.softmax(word_distribution).argmax(dim=1)
            pred_words.append(single_pred_word)
            hist_att_weights.append(attn_weights.detach())
            w_emb = self.word_embeddings(single_pred_word)
            seq_len += 1
        return torch.stack(pred_words).permute(1, 0), torch.stack(hist_att_weights).permute(1, 0, 2)

    def predict_single_word(self, w_emb, feature_maps, h, c):
        att_feature_maps_weights = self.attention_layer(feature_maps, h.squeeze(0))
        lstm_input = torch.cat([att_feature_maps_weights, w_emb], dim=1).unsqueeze(0)
        out, (h, c) = self.lstm(lstm_input, (h, c))
        out = self.dropout(out)
        word_distribution = self.vocab_fc(out.squeeze(0))
        return word_distribution, h, c, att_feature_maps_weights

    def get_lstm_initial_state(self, feature_maps):
        mean_feature = torch.mean(feature_maps, dim=1)
        h0 = self.init_fc_h(mean_feature).unsqueeze(0)
        c0 = self.init_fc_c(mean_feature).unsqueeze(0)
        return h0, c0

    def add_bos_token(self, text):
        bs = text.shape[0]
        pad_idxs = self.get_bos_idx_sample(bs=bs)
        return torch.cat([pad_idxs, text], dim=1)

    def get_bos_idx_sample(self, bs):
        bos_idx = 2
        return (torch.zeros(bs, 1, dtype=torch.long) + bos_idx).to(self.device)


class AttentionCaptionGenerator(nn.Module):
    def __init__(self, encoder, decoder):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, X):
        idx, img, text = X
        feature_maps = self.encoder.forward(img)
        word_distributions = self.decoder.forward(feature_maps, text)
        return word_distributions

    def sample(self, img):
        feature_maps = self.encoder.forward(img)
        word_distributions, attn_hist = self.decoder.sample(feature_maps)
        return word_distributions, attn_hist, feature_maps

    @classmethod
    def from_config(
            cls,
            vocab_size,
            attention_dim,
            word_emb_dim,
            hidden_lstm_dim,
            feature_maps_nb,
            voc_fc_dropout,
            encoded_image_size,
            **kwargs
    ):
        encoder = VggEncoder(encoded_image_size)
        decoder = AttentionDecoder(
            attention_dim=attention_dim,
            word_emb_dim=word_emb_dim,
            hidden_lstm_dim=hidden_lstm_dim,
            vocab_size=vocab_size,
            feature_maps_nb=feature_maps_nb,
            voc_fc_dropout=voc_fc_dropout,
        )
        return cls(
            encoder=encoder,
            decoder=decoder,
        )
