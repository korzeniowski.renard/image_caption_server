import boto3
import config


def get_s3_client():
    return boto3.client(
        "s3",
        region_name=config.region,
    )


def save_to_s3(obj, obj_id):
    s3 = get_s3_client()
    s3.put_object(
        Bucket=config.bucket_name,
        Key=obj_id,
        Body=bytearray(obj),
    )


def load_from_s3(bucket_key):
    s3 = get_s3_client()
    response = s3.get_object(
        Bucket=config.bucket_name,
        Key=bucket_key,
    )

    data = response["Body"].read()
    return data


def download_model_from_s3():
    s3 = boto3.resource(
        "s3",
        region_name=config.region,
    )
    bucket = s3.Bucket(config.bucket_name)
    bucket.download_file(config.model_bucket_key, config.model_save_path)
