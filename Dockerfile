FROM python:3.6
WORKDIR /server
RUN apt-get update && apt-get install -y --no-install-recommends \
	python-opencv
COPY ./requirements.txt .
RUN pip install -r requirements.txt --no-cache-dir
COPY . .
ENTRYPOINT [ "python3" ]
CMD [ "server.py" ]
