from flask import abort
from flask import request
from flask_restplus import Resource
import requests
from threading import Thread

import config
from src import server_exceptions
from src.controllers.image_upload import save_predictions, get_img
from src.namespaces.image_upload import api


class MakePredictions(Thread):
    def __init__(self, request):
        Thread.__init__(self)
        self.request = request

    def run(self):
        img_bytes, db_img_id = get_img(self.request)
        prediction = save_predictions(img_bytes, db_img_id)
        print("Successful prediction")
        requests.post(config.frontend_url, data=prediction)


@api.route("")
class ImageUpload(Resource):
    @api.doc("Upload image to the server database")
    def post(self):
        try:
            model_thread = MakePredictions(request.__copy__())
            model_thread.start()
            return "Processing in the background", 200
        except server_exceptions.ImgProcessingException as error:
            abort(422, str(error))
        except Exception as error:
            abort(400, str(error))
