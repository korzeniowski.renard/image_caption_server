import os

region = "eu-central-1"
bucket_name = "image-descriptor-app-testing"
frontend_secret_key = os.getenv('FRONTEND_SECRET_KEY', None)
object_type = ".jpg"
save_image_folder = "attention_images"
model_bucket_key = "inference_checkpoint.pytorch"
model_save_path = "inference_checkpoint.pytorch"
frontend_url = "http://webloadbalancer-1713583670.eu-central-1.elb.amazonaws.com/api/add_description/"
max_pred_seq_len = 20
