import cv2
import numpy as np

from src import server_exceptions


def load_img_from_bytes(img_bytes):
    # np.float32 does not work with .png files
    npimg = np.fromstring(img_bytes, np.uint8)
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    if img is None:
        raise server_exceptions.ImgProcessingException(
            "Server was not able to load provided image")
    return img
