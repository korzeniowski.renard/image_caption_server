import uuid

import config
from src.services.image_upload import load_img_from_bytes
from src.model_pipeline import inference
from src import utilities

prediction_pipeline = inference.PredictionPipeline.from_s3()


def save_attention_maps(predictions, obj_id, db_img_id):
    urls = {
        "secret_key": config.frontend_secret_key,
        "image_id": db_img_id,
        "text": [],
        "pictures": []
    }

    for i, (text, img) in enumerate(predictions):
        prediction_id = obj_id + "_" + str(i) + config.object_type
        prediction_id = config.save_image_folder + "/" + prediction_id
        utilities.save_to_s3(img, prediction_id)
        urls["text"].append(text)
        urls["pictures"].append(prediction_id)
    return urls


def get_img(request):
    bucket_key, db_img_id = request.form.getlist('bucket_key')[0], request.form.getlist('image_id')[0]
    img_bytes = utilities.load_from_s3(bucket_key)
    return img_bytes, db_img_id


def save_predictions(img_bytes, db_img_id):
    obj_id = str(uuid.uuid4())
    img = load_img_from_bytes(img_bytes)
    predictions = prediction_pipeline.get_predictions(img)
    response_urls = save_attention_maps(
        predictions=predictions,
        obj_id=obj_id,
        db_img_id=db_img_id,
    )
    return response_urls
