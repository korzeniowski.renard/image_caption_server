import torch
from torchvision import transforms
import skimage.transform
import numpy as np
from PIL import Image
import cv2

from src import utilities
import config
from src.model_pipeline import model_architecture
from src.model_pipeline import text_processing


class PredictionPipeline:
    def __init__(self, model, vocab, image_transform):
        self.model = model
        self.vocab = vocab
        self.image_transform = image_transform
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    def get_predictions(self, img):
        img = Image.fromarray(img)
        img = self.image_transform(img).unsqueeze(0)
        img = img.to(self.device)
        pred_words, attn_hist, feature_maps = self.get_single_prediction(img)
        combined_imgs = []
        for i in range(len(pred_words)):
            feature_map_weigths = attn_hist[0, i]
            heat_map = feature_map_weigths.dot(feature_maps)
            heat_map = (heat_map - heat_map.min()) / (heat_map.max() - heat_map.min())
            heat_map = skimage.transform.resize(heat_map.reshape(7, 7), img.shape[2:4])
            heat_map = (heat_map * 255).astype(np.uint8)

            norm_img = (img - img.min()) / (img.max() - img.min())
            norm_img = (norm_img[0].permute(1, 2, 0).numpy() * 255).astype(np.uint8)

            heat_map = cv2.applyColorMap(heat_map, cv2.COLORMAP_JET)
            combined_img = (1 - 0.4) * norm_img + 0.4 * heat_map
            combined_img = combined_img.astype(np.uint8)
            status, combined_img = cv2.imencode(".jpg", combined_img)
            combined_imgs.append(combined_img)

        predictions = [(txt, img) for txt, img in zip(pred_words, combined_imgs)]
        return predictions

    def get_single_prediction(self, img):
        with torch.no_grad():
            y_hat, attn_hist, feature_maps = self.model.sample(img)
            pred_words = self.word_idx_to_text(y_hat[0].cpu()).split()
            feature_maps = feature_maps[0].T.cpu().numpy()
            attn_hist = attn_hist.numpy()
        return pred_words, attn_hist, feature_maps

    def word_idx_to_text(self, word_idxs):
        words = self.vocab.textify(word_idxs)
        words = words.replace(" xxpad", "").replace(" xxeos", "")
        return words

    @classmethod
    def from_s3(cls):
        utilities.download_model_from_s3()
        checkpoint = torch.load(config.model_save_path)
        vocab = text_processing.Vocab(itos=checkpoint["vocab"]["itos"])
        model = model_architecture.AttentionCaptionGenerator.from_config(
            vocab_size=vocab.get_vocab_size(),
            **checkpoint["config"]
        )
        model.load_state_dict(checkpoint["model"], strict=True)
        model.eval()
        return cls(
            model=model,
            image_transform=cls.get_default_transformations(**checkpoint["config"]),
            vocab=vocab,
        )

    @staticmethod
    def get_default_transformations(image_resize, image_crop, image_normalization, image_std, **kwargs):
        return transforms.Compose([
            transforms.Resize(image_resize),
            transforms.CenterCrop(image_crop),
            transforms.ToTensor(),
            transforms.Normalize(mean=image_normalization, std=image_std),
        ])
