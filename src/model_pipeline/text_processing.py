import collections
import re

import spacy


BOS, EOS, UNK, PAD = "xxbos", "xxeos", "xxunk", "xxpad"
TK_MAJ, TK_UP, TK_REP, TK_WREP = "xxmaj", "xxup", "xxrep", "xxwrep"
TEXT_SPEC_TOK = [UNK, PAD, BOS, EOS, TK_MAJ, TK_UP, TK_REP, TK_WREP]


def spec_add_spaces(t):
    "Add spaces around / and # in `t`. \n"
    return re.sub(r"([/#\n])", r" \1 ", t)


def rm_useless_spaces(t):
    "Remove multiple spaces in `t`."
    return re.sub(" {2,}", " ", t)


def replace_rep(t):
    "Replace repetitions at the character level in `t`."

    def _replace_rep(m):
        c, cc = m.groups()
        return f" {TK_REP} {len(cc) + 1} {c} "

    re_rep = re.compile(r"(\S)(\1{3,})")
    return re_rep.sub(_replace_rep, t)


def replace_wrep(t):
    "Replace word repetitions in `t`."

    def _replace_wrep(m):
        c, cc = m.groups()
        return f" {TK_WREP} {len(cc.split()) + 1} {c} "

    re_wrep = re.compile(r"(\b\w+\W+)(\1{3,})")
    return re_wrep.sub(_replace_wrep, t)


def replace_all_caps(x):
    "Replace tokens in ALL CAPS in `x` by their lower version and add `TK_UP` before."
    res = []
    for t in x:
        if t.isupper() and len(t) > 1: res.append(TK_UP); res.append(t.lower())
        else: res.append(t)
    return res


def deal_caps(x):
    "Replace all Capitalized tokens in `x` by their lower version and add `TK_MAJ` before."
    res = []
    for t in x:
        if t == "": continue
        if t[0].isupper() and len(t) > 1 and t[1:].islower(): res.append(TK_MAJ)
        res.append(t.lower())
    return res


TEXT_PRE_RULES = [replace_rep, replace_wrep, spec_add_spaces, rm_useless_spaces]
TEXT_POST_RULES = [replace_all_caps, deal_caps]


class Tokenizer:
    def __init__(self):
        self.tok_fn = spacy.load("en_core_web_sm", disable=["parser", "tagger", "ner"])
        self.pre_special_cases = TEXT_PRE_RULES
        self.post_special_cases = TEXT_POST_RULES

    def process_text(self, text):
        for pre_case in self.pre_special_cases: text = pre_case(text)
        tokens = self.tokenize(text)
        for post_case in self.post_special_cases: tokens = post_case(tokens)
        return tokens

    def tokenize(self, text):
        return [tok.text for tok in self.tok_fn(text)]


class Vocab:
    """FastAI https://github.com/fastai/fastai/blob/master/fastai/text/transform.py#L128"""
    def __init__(self, itos):
        self.itos = itos
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})

    def get_vocab_size(self):
        return len(self.itos)

    def numericalize(self, t):
        return [self.stoi[w]for w in t]

    def textify(self, nums, sep=" "):
        return sep.join([self.itos[i] for i in nums])

    def __getstate__(self):
        return {"itos": self.itos}

    def __setstate__(self, state:dict):
        self.itos = state["itos"]
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})

    @classmethod
    def from_tokens(cls, tokens, max_vocab, min_freq):
        freq = collections.Counter(p for o in tokens for p in o)
        itos = [o for o, c in freq.most_common(max_vocab) if c >= min_freq]
        for o in reversed(TEXT_SPEC_TOK):
            if o in itos: itos.remove(o)
            itos.insert(0, o)
        itos = itos[:max_vocab]
        return cls(itos)
