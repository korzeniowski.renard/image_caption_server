from flask_restplus import Namespace

api = Namespace(
    "image_upload",
    description="Upload image and get list of object keys for a predefined S3 bucket.",
    path="/image_upload"
)
