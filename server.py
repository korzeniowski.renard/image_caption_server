from flask import Flask
from flask_cors import CORS
from flask_restplus import Api

from src.routers import image_upload


def create_app():
    app = create_server()
    create_api(app)
    return app


def create_server():
    app = Flask(__name__)
    CORS(app)
    return app


def create_api(app):
    api = Api(
        title="Image caption generator",
        version="0.0",
        description="Server that describes images and shows attended elements for predicted words",
    )
    api.add_namespace(image_upload.api)
    api.init_app(app)


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=5000, debug=True)
