from werkzeug.datastructures import ImmutableMultiDict

import config
from src import utilities


def test_get_image(monkeypatch):
    expected_output = None, 420

    class MockRequest:
        def __init__(self, form):
            self.form = form

    request_mock = MockRequest(ImmutableMultiDict({'bucket_key': ["test_bucket_key"], 'image_id': [420]}))

    def mock_load_s3(bucket_key):
        return None

    monkeypatch.setattr(utilities, "load_from_s3", mock_load_s3)

    from src.model_pipeline import inference

    class MockPredictionPipeline:
        @classmethod
        def from_s3(cls, **kwargs):
            return cls()

    monkeypatch.setattr(inference, "PredictionPipeline", MockPredictionPipeline)

    from src.controllers.image_upload import get_img
    test_output = get_img(request=request_mock)

    assert expected_output == test_output


def test_save_attention_maps(monkeypatch):
    mocked_predictions = [("text_1", "mock_img_1"), ("text_2", "mock_img_2")]
    mocekd_obj_id = "test_obj_id"
    mocekd_db_img_id = 2

    expected_output = {
        "secret_key": config.frontend_secret_key,
        "image_id": mocekd_db_img_id,
        "text": ["text_1", "text_2"],
        "pictures": ["attention_images/test_obj_id_0.jpg", "attention_images/test_obj_id_1.jpg"]
    }

    def mock_save_to_s3(img, prediction_id):
        pass

    monkeypatch.setattr(utilities, "save_to_s3", mock_save_to_s3)

    from src.controllers.image_upload import save_attention_maps

    test_output = save_attention_maps(
        predictions=mocked_predictions,
        obj_id=mocekd_obj_id,
        db_img_id=mocekd_db_img_id
    )

    assert expected_output["secret_key"] == test_output["secret_key"]
    assert expected_output["image_id"] == test_output["image_id"]
    assert expected_output["text"][0] == test_output["text"][0]
    assert expected_output["text"][1] == test_output["text"][1]
    assert expected_output["pictures"][0] == test_output["pictures"][0]
    assert expected_output["pictures"][1] == test_output["pictures"][1]
